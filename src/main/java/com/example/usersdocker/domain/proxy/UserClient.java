package com.example.usersdocker.domain.proxy;

import com.example.usersdocker.entity.User;

import java.util.List;

public interface UserClient {
    List<User> getUsers(String mensajeTransaccion, String idTransaccion);
}
