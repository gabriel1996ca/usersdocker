package com.example.usersdocker.domain.proxy;

import com.example.usersdocker.entity.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserClientImpl implements UserClient {

    private final RestTemplate restTemplate;

    public UserClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<User> getUsers(String mensajeTransaccion, String idTransaccion) {
        String url = "https://jsonplaceholder.typicode.com/todos/";
        ResponseEntity<User[]> responseEntity = restTemplate.getForEntity(url, User[].class);
        return Arrays.stream(Objects.requireNonNull(responseEntity.getBody())).collect(Collectors.toList());
    }
}