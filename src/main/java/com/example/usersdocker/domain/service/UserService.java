package com.example.usersdocker.domain.service;

import com.example.usersdocker.entity.User;

import java.util.List;

public interface UserService {
    List<User> getUsers(String idTransaccion, String idAplicacion);
}